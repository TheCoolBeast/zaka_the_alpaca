#!/bin/bash
sign=false
install=false

while test $# -gt 0; do
    case "$1" in
        -h|--help)
            echo "Zaka the Alpaca builder"
            echo " "
            echo "build.sh [options] application [arguments]"
            echo " "
            echo "options:"
            echo "-h, --help                show brief help"
            echo "-s, --sign                sign the App"
            echo "-i, --install             install the App"
            exit 0
            ;;
        -s|--sign)
            sign=true
            shift
            ;;
        -i|--install)
            install=true
            shift
            ;;
        *)
            break
            ;;
    esac
done

echo "========================================"
echo "Building Zaka the Alpaca for Android..."
echo "========================================"

#Reset android platform
cordova platform rm android
cordova platform add android
cordova build android

#Copy resources and manifest
cp www/res/icons/android/icon-36-ldpi.png platforms/android/res/drawable-ldpi/icon.png
cp www/res/icons/android/icon-48-mdpi.png platforms/android/res/drawable-mdpi/icon.png   
cp www/res/icons/android/icon-72-hdpi.png platforms/android/res/drawable-hdpi/icon.png   
cp www/res/icons/android/icon-96-xhdpi.png platforms/android/res/drawable-xhdpi/icon.png   
cp www/res/AndroidManifest.xml platforms/android/

#Run ant script
cd platforms/android
rm -R bin gen
ant release

#Sign the apk
if $sign;
then
    echo "Signing..."
    cd bin
    jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ~/.keystore/zaka-the-alpaca-release.keystore ZakatheAlpaca-release-unsigned.apk alpaca

    #Zipalign
    zipalign -v 4 ZakatheAlpaca-release-unsigned.apk ZakaTheAlpaca.apk

    #Install
    if $install;
    then
        echo "Installing onto device..."
        adb install ZakaTheAlpaca.apk
    fi

else
    if $install;
    then
        echo "ERROR: App can only be installed if it's been previously signed."
    fi
fi

