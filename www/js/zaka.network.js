//  Authors:
//        Daniel Olea Martín < dom.olea@gmail.com >
//        Bruno Ordozgoiti Rubio < ordozgoitipiedras@gmail.com >
//        Copyright (c) 2013 Zaka the Alpaca
//        All Rights Reserved. 
//        
//        BANANAPPS CONFIDENTIAL
//        __________________ 
//        NOTICE:  All information contained herein is, and remains
//        the property of Bananapps and its suppliers, if any.The 
//        intellectual and technical concepts contained  herein are 
//        proprietary to Bananapps and its suppliers and may be 
//        covered by U.S.and Foreign Patents, patents in process,
//        and are protected by trade secret or copyright law.
//        Dissemination of this information or reproduction of this 
//        material is strictly forbidden unless prior written 
//        permission is obtained from Bananapps.
//        
//        

(function(zaka, $, undefined) {
    
    /* Alpaca's JSON version. Must be changed if the JSON is modified to the extent
       of being incompatible with prior versions.*/
    zaka.COMM_VERSION = 1;

    /* If our server is unreachable, try with merrian*/
    function fallback(word, filler) {
        $.ajax({
            url: "http://www.dictionaryapi.com/api/v1/references/collegiate/xml/" + word + "?key=2143502c-9a81-422c-95c3-d565195a8992",
            type: "GET",
            dataType: "text" //Ugghh...
            
        }).done(function(data) {
            var matches = data.match(/<dt>.*?<\/dt>/gi);

            var json = {
                'word': word,
                'definitions': []
            };

            if (matches !== null) {
                var i = 0;
                while (i < matches.length) {
                    /* .html() is needed to avoid clean() removing
                     * everything since the result is inside a <dt> */
                    var def = zaka.ui.clean($(matches[i]).html());

                    //Merrian webster...
                    if (def !== '.') {
                        json.definitions.push({'definition': def});
                    }

                    i++;
                }
            }
            
            filler(json);
            
        }).fail(function() {
            zaka.ui.displayMsg(
                "Please, check your internet connection.",
                "Connection unavailable",
                "Ok",
                function() {
                    window.history.back();
                }
            );
        })
    }

    /**
     * Public network functionalities
     */
    zaka.network = {
        /**
         * Looks for the definition of a word. If our server is down, it will
         * look for it in Merrian dictionary.
         * @param {String} word
         * @param {function} filler(data: JSON). JSON -> {word, definitions: [{definition, type}, ...]}
         */
        define: function(word, filler) {
            //word = word.split(' ')[0].toLowerCase();
            $.ajax({
                url: "http://monom.io/alpaca-api/en/define.php?query=" + word +
                    "&v=" + zaka.COMM_VERSION,
                type: "GET",
                dataType: "text"//000Webhost!!

            }).done(function(data) {
                //000Webhost niapa
                data = '(' + data + '/**/)';
                try {
                    //alert(data);
                    filler(eval(data));
                } catch (e) {
                    //eval will throw an exception if the result is not valid js (e.g. 'DB error')
                    fallback(word, filler);
                }

            }).fail(function() {
                fallback(word, filler);
            });
        }
    }
}(window.zaka = window.zaka || {}, jQuery));
