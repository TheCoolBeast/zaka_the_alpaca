/*
 Authors:
 Daniel Olea Martín < dom.olea@gmail.com >
 Bruno Ordozgoiti Rubio < ordozgoitipiedras@gmail.com >
 Copyright (c) 2013 Zaka the Alpaca
 All Rights Reserved.
 
 BANANAPPS CONFIDENTIAL
 __________________ 
 NOTICE:  All information contained herein is, and remains
 the property of Bananapps and its suppliers, if any.The 
 intellectual and technical concepts contained  herein are 
 proprietary to Bananapps and its suppliers and may be 
 covered by U.S.and Foreign Patents, patents in process,
 and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this 
 material is strictly forbidden unless prior written 
 permission is obtained from Bananapps.
 */

(function(zaka, $, undefined) {

    /*
     * Renders possible on-tap feedback, the element just needs to 
     * have the "data-tappable" attr. set to true.
     */
    $(document).ready(function(){
        $("[data-tappable=true]").on('touchstart', function(){
            $(this).addClass('tapped'); 
        });

        $("[data-tappable=true]").on('touchend', function(){
            var target = this;
            window.setTimeout(
                function(){$(target).removeClass('tapped')},
                100
            );
        });
    })
    
    /**
     * Public user interface functionalities
     */
    zaka.ui = {
                
        /**
         * Makes the first letter a capital letter.
         * @param {type} string
         * @returns {undefined} capitalized string
         */
        capitalize: function(string) {
            return string.charAt(0).toUpperCase() + string.slice(1)
        },
        /**
         * Removes all colons (:) and all xml tags included inside (and
         * their contents). It also adds a necessary dot (.) at the end of 
         * the sentence.
         * @param {String} xml
         * @returns {String} cleaned string.
         */
        clean: function(xml) {
            var cleaned = xml.replace(/(<.*\/[a-z]*>|:)/gi, '').trim();
            return this.capitalize(cleaned) + '.';
        },
        /**
         * Shows a message to the user
         * @param {String} title Optional. Title of the message.
         * @param {String} text message
         * @param {String} buttonText Optional. Text to display in the button of
         * acknowledgement.
         * @param {function} callback Optional: to be executed once the user has
         * acknowledge the message.
         */
        displayMsg: function(text, title, buttonText, callback) {
            alert(text);
        },
        /**
         * Enables scrolling of an element. The element must be child of another
         * element with 'overflow' set to 'hidden'.
         * The scrolled element must be position: absolute or relative.
         * @param {JQuery selector (String)} target of the scrolling
         */
        scroll: function(target) {
            var currentTop = 0, scrollStartPos = 0;
            
            $("body").on('touchstart', target, function(event) {
                scrollStartPos = event.originalEvent.touches[0].pageY;
                currentTop = parseInt($(target).css('top'), 10);
            });
            
            $("body").on('touchmove', target, function(event) {
                event.originalEvent.preventDefault();

                var targetTotalHeight = $(target).height() + 
                        parseInt($(target).css('margin-top'), 10) +
                        parseInt($(target).css('margin-bottom'), 10) +
                        parseInt($(target).css('padding-top'), 10) +
                        parseInt($(target).css('padding-bottom'), 10);
                
                var parentHeight = $(target).parent().height();
                
                //Do we need scrolling?
                if(targetTotalHeight > parentHeight){
                    var newPoss = currentTop - (scrollStartPos - event.originalEvent.touches[0].pageY);

                    var min =  - (targetTotalHeight - parentHeight);

                    //Prevent list flight
                    if(newPoss < min){
                        newPoss = min;
                    }else if(newPoss > 0){
                        newPoss = 0;
                    }

                    $(target).css({'top' : newPoss});
                }
            });
        }

    };
}(window.zaka = window.zaka || {}, jQuery));