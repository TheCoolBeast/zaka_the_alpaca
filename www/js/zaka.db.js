/*
 Authors:
 Daniel Olea Martín < dom.olea@gmail.com >
 Bruno Ordozgoiti Rubio < ordozgoitipiedras@gmail.com >
 Copyright (c) 2013 Zaka the Alpaca All Rights Reserved.
 
 BANANAPPS CONFIDENTIAL
 __________________ 
 NOTICE:  All information contained herein is, and remains
 the property of Bananapps and its suppliers, if any.The 
 intellectual and technical concepts contained  herein are 
 proprietary to Bananapps and its suppliers and may be 
 covered by U.S.and Foreign Patents, patents in process,
 and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this 
 material is strictly forbidden unless prior written 
 permission  is obtained from Bananapps.
 */

(function(zaka, $, undefined) {

    //TODO: onReady??
    //Create the 1MB database
    var db = window.openDatabase("Zaka", "1.0", "Zaka", 1000 * 1024);

    //Create its schema
    db.transaction(createDB, onError);

    //Reports an error to the user
    function onError(error) {
        alert("The following error was encountered, please contact the" +
                " development team: " + error);
        console.log("zaka.db error: " + error);
    }

    /*
     * Creates the database.
     * @param {function} tx
     */
    function createDB(tx) {
        tx.executeSql(
            'CREATE TABLE IF NOT EXISTS Word' +
                ' (word unique, last_time_showed)', 
            [], 
            null, 
            function(tx, error){
                onError("createDB 1 - " + error.message);
            }
        );

        tx.executeSql(
            'CREATE TABLE IF NOT EXISTS Definition' +
                ' (word_fk, definition)', 
            [], 
            null, 
            function(tx, error){
                onError("createDB 2 - " + error.message);
            }
        );

    }

    /*
     * Inserts the definitions of a word in the DB. When finished it calls the
     * onSuccess function.
     * @param {String} word
     * @param {Array} definitions
     * @param {function} tx
     * @param {function} onSuccess callback on success
     */
    function insertDefinitions(word, definitions, tx, onSuccess) {
        //Create the sql VALUE and the parameters of the query
        var values = "";
        var params = new Array();
        for (var i = 0; i < definitions.length; i++) {
            values += "select ?, ?";
            if (i + 1 !== definitions.length) {
                values += ' union all ';
            }

            //Order matters
            params.push(word);
            params.push(definitions[i]);
        }

        //Insert the definition and notify once it is done.
        tx.executeSql(
            "INSERT INTO Definition (word_fk, definition)"
                + values,
            params,
            onSuccess,
            function(tx, error){
                onError("insertDefinitions - " + error.message);
            }
        )
    }

    /**
     * Public persistance utilities
     */
    zaka.db = {
        //TODO: nonsense! better to display selected definitions and simply del + insert
        /**
         * Saves a word and its definitions to the db. <b>If the word was already
         * present in the db, only the new definitions will be added.</b>
         * @param {String} word
         * @param {Array} definitions 
         * @param {function} onSuccess it will be executed once the vocab has been
         * saved to the database.
         */
        saveVocab: function(word, definitions, onSuccess) {
            db.transaction(function(tx) {
                tx.executeSql(
                        'SELECT word_fk, definition FROM Definition WHERE word_fk = ?',
                        [word],
                        function(tx, rs) {
                            //If the word already exists
                            if (rs.rows.length > 0) {
                                var newDefinitions = new Array();

                                //Insert only new definitions
                                for (var i = 0; i < definitions.length; i++) {
                                    var insert = true;
                                    for (var j = 0; j < rs.rows.length; j++) {
                                        //TODO: test if comparing the hash is faster
                                        if (rs.rows.item(j).definition === definitions[i]) {
                                            insert = false;
                                        }
                                    }

                                    if (insert) {
                                        newDefinitions.push(definitions[i]);
                                    }

                                    if (newDefinitions.length > 0) {
                                        insertDefinitions(
                                            word,
                                            newDefinitions,
                                            tx,
                                            onSuccess
                                        );
                                    } else {
                                        //Let's suppose it is a function
                                        if (onSuccess !== undefined) {
                                            onSuccess();
                                        }
                                    }
                                }

                            } else {
                                //Insert the word
                                tx.executeSql(
                                        "INSERT INTO Word (word, last_time_showed)"
                                        + " VALUES (?, 0)",
                                        [word],
                                        null,
                                        function(tx, error){
                                            onError("saveVocab 2 - " + error.message);
                                        }
                                        )

                                //Insert the definitions
                                insertDefinitions(word, definitions, tx, onSuccess);
                            }
                        },
                        function(tx, error){
                            onError("saveVocab 1 - " + error.message);
                        });
            }, function(error){
                onError("saveVocab 1 - " + error.message);
            });
        },
        
        //TODO: synchronus?
        /**
         * Counts the number of words the user has stored.
         * @param {type} callback function with <b>one parameter</b>. The number
         * returned will be set in that parameter.
         */
        countWords: function(callback) {
            db.transaction(function(tx) {
                tx.executeSql(
                    "SELECT count(word) AS numWords FROM Word",
                    [],
                    function(tx, rs) {
                        callback(rs.rows.item(0).numWords);
                    },
                    null,//onSuccess
                    function(tx, error){
                        onError("countWords - " + error.message);
                    }
                );
            }, 
            function(error){
                onError("countWords - " + error.message);
            });
        },
        
        //TODO: synchronus?
        /**
         * Returns <i>limit</i> words order by <i>last_time_showed</i>.
         * @param {int} limit number of words to return.
         * @param {function} callback function with one parameter, an array as
         * follows: [word1, word2...]
         * @param {String} notThisWord Optional. Word to avoid.
         */
        getOrderedWords: function(limit, callback, notThisWord) {
            notThisWord = notThisWord || '';
            db.transaction(function(tx) {
                tx.executeSql(
                    "SELECT word FROM Word WHERE word != ? ORDER BY last_time_showed ASC LIMIT ? ",
                    [notThisWord, limit],
                    function(tx, rs) {
                        var vocabs = new Array();
                        for (var i = 0; i < rs.rows.length; i++) {
                            vocabs.push(rs.rows.item(i).word);
                        }

                        if (callback !== undefined) {
                            callback(vocabs);
                        }
                    },
                    function(tx, error){
                        onError("getOrderedWords - " + error.message);
                    }
                );
            }, 
            function(error){

                onError("getOrderedWords - " + error.message);
            })
        },
        
        //TODO: synchronus?
        /**
         * Returns the definition fo the word.
         * @param {String} word.
         * @param {function} callback function with one parameter, an array as
         * follows: [definition1, definition2...]
         */
        getDefinition: function(word, callback) {

            db.transaction(function(tx) {
                tx.executeSql(
                    "SELECT definition FROM Definition WHERE word_fk = ?",
                    [word],
                    function(tx, rs) {
                        var defs = new Array();
                        for (var i = 0; i < rs.rows.length; i++) {
                            defs.push(rs.rows.item(i).definition);
                        }

                        if (callback !== undefined) {
                            callback(defs);
                        }
                    },
                    function(tx, error){
                        onError("getDefinition 1 - " + error.message);
                    }
                );
            }, 
            function(error){
                onError("getDefinition 2 - " + error.message);
            })
        },
        
        /**
         * Mark a word as showed
         * @param {String} word         
         */
        markAsShowed: function(word) {
            db.transaction(function(tx) {
                tx.executeSql(
                    "UPDATE Word SET last_time_showed = ? WHERE word = ?",
                    [new Date().getTime(), word],
                    null, //onSuccess
                    function(tx, error){
                        onError("markAsShowed - " + error.message);
                    }
                );
            }, 
            function(error){
                onError("markAsShowed - " + error.message);
            })
        }

    }

}(window.zaka = window.zaka || {}, jQuery));