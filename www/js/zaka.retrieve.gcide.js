//Authors:
//        Daniel Olea Martín < dom.olea@gmail.com >
//        Bruno
//        Ordozgoiti Rubio < ordozgoitipiedras@gmail.com >
//        Copyright (c) 2013 Zaka the Alpaca
//        All
//        Rights Reserved.
//        BANANAPPS
//CONFIDENTIAL
//        __________________ 
//        NOTICE:  All information contained herein is, and remains
//        the property of Bananapps and its suppliers, if any.The 
//        intellectual and technical concepts contained  herein are 
//        proprietary to Bananapps and its suppliers and may be 
//        covered by U.S.and Foreign Patents, patents in process,
//        and are protected by trade secret or copyright law.
//        Dissemination of this information or reproduction of this 
//        material is strictly forbidden unless prior written 
//        permission
//is obtained from Bananapps.
//        
//        

zaka.onReady(function() {
    var word = zaka.communication.receive("word");
    
    //TODO niapa para coger solamente la primera palabra.
    //No admite PVs ni idioms, de momento
    word = word.split(' ')[0].toLowerCase();

    $.ajax({
        url: "http://www.dictionaryapi.com/api/v1/references/collegiate/xml/" + word + "?key=2143502c-9a81-422c-95c3-d565195a8992",
        type: "GET",
        dataType: "text", //Ugghh...
        success: filler,
        error: function(error) {
            zaka.ui.displayMsg(
                    "Please, check your internet connection.",
                    "Connection unavailable",
                    "Ok",
                    function() {
                        window.history.back();
                    }
            );
        }
    });

    //Capitalize (css text-transform goes mad)
    $("header span").html(zaka.ui.capitalize(word));

    //Mark as selected on click
    $("ul").on("click", "li", function() {
        $(this).toggleClass("green");
    });

    $("#cancel").click(function() {
        window.history.back();
    });

    $("#save").click(function() {
        var definitions = new Array();

        //Get all definitions selected
        var selected = $(".elejible.green");
        selected.each(function() {
            definitions.push($(this).text());
        });

        if (selected.length > 0) {
            //save the word and its definitions
            zaka.db.saveVocab(word, definitions, function() {
                //Feedback
                $("#save").addClass("saved");

                var autoWidth = $("#saved_feedback").width();
                $("#saved_feedback").css({
                    display: 'inline',
                    width: 0
                });

                $("#saved_feedback")
                        .animate(
                                {width: autoWidth},
                        250
                                ).delay(750)
                        .animate(
                                {opacity: 0},
                        350
                                ).delay(1000)
                        .queue(function(next) {
                            window.history.back();
                            next();
                        });
            });
        } else {
            zaka.ui.displayMsg(
                    "Select a definition to save.",
                    "No definition selected",
                    "Ok"
                    );
        }
    });
});


//TODO We might have to adapt this design to the search of
//idioms and phrasal verbs
//Different functions should extract each possibility (word, PV, idiom)
//Which function is called should be determined upon finding out what
//are we dealing with, but the ajax request is kind of a bottleneck

/**
 * Parses the GCIDE XML and fills the definition list
 * @param {String} data definition.
 */
function filler(data) {
    $("#loader").remove();
    
    var matches = data.match(/<dt>.*?<\/dt>/gi);

    if (matches !== null) {
        var max = matches.length < 5 ? matches.length : 5;
        var i = 0;
        while (i < max) {
            /* .html() is needed to avoid clean() removing
             * everything since the result is inside <dt> */
            var def = zaka.ui.clean($(matches[i]).html());

            //Merrian webster...
            if (def !== '.') {
                $("ul").append(
                        ich.definition({
                            definition: def
                        })
                        );

            } else {
                //Since this match was invalid we put another one in.
                if (max + 1 <= matches.length) {
                    max++;
                }
            }

            i++;
        }
        ;
    } else {
        zaka.ui.displayMsg(
                "It seems that the word has no entry in the dictionary." +
                " Please, check the spelling.",
                "Word not found",
                "Ok"
                );
    }
}

//TODO: remove jancho debugging
$(document).ready(function() {
    if (!zaka.isPhone()) {
        filler('<entry_list version="1.0"><entry id="dwelling"><ew>dwelling</ew><subj>AC</subj><hw>dwell*ing</hw><fl>noun</fl><def><date>14th century</date><dt>:a shelter (as a house) in which people live</dt></def></entry><entry id="dwell"><ew>dwell</ew><hw>dwell</hw><sound><wav>dwell001.wav</wav><wpr>!dwel</wpr></sound><pr>ˈdwel</pr><fl>verb</fl><in><if>dwelled</if><sound><wav>dwell003.wav</wav><wpr>!dwelt</wpr><wav>dwell002.wav</wav><wpr>!dweld</wpr></sound><pr>ˈdweld, ˈdwelt</pr><il>or</il><if>dwelt</if><sound><wav>dwell003.wav</wav><wpr>!dwelt</wpr></sound><pr>ˈdwelt</pr></in><in><if>dwelling</if></in><et>Middle English, from Old English <it>dwellan</it> to go astray, hinder; akin to Old High German <it>twellen</it> to tarry</et><def><vt>intransitive verb</vt><date>13th century</date><sn>1</sn><dt>:to remain for a time</dt><sn>2 a</sn><dt>:to live as a resident</dt><sn>b</sn><dt>:<sx>exist</sx><sx>lie</sx></dt><sn>3 a</sn><dt>:to keep the attention directed <un>used with <it>on</it> or <it>upon</it><vi>tried not to <it>dwell</it> on my fears</vi></un></dt><sn>b</sn><dt>:to speak or write insistently <un>used with <it>on</it> or <it>upon</it><vi>reporters <it>dwell</it><it>ing</it> on the recent scandal</vi></un></dt></def><uro><ure>dwell*er</ure><fl>noun</fl></uro></entry><entry id="lake dwelling"><ew>lake dwelling</ew><subj>AR</subj><hw>lake dwelling</hw><fl>noun</fl><def><date>1863</date><dt>:a dwelling built on piles in a lake</dt><sd>specifically</sd><dt>:one built in prehistoric times</dt></def><uro><ure>lake dweller</ure><fl>noun</fl></uro></entry><entry id="cliff dweller"><ew>cliff dweller</ew><subj>AI-1a#AR-1a#AP-1b</subj><hw>cliff dweller</hw><fl>noun</fl><def><date>1881</date><sn>1</sn><slb>often capitalized C&D</slb><sn>a</sn><dt>:a member of a prehistoric American Indian people of the southwestern United States who built their homes on rock ledges or in the natural recesses of canyon walls and cliffs</dt><sn>b</sn><dt>:a member of any cliff-dwelling people</dt><sn>2</sn><dt>:a resident of a large usually metropolitan apartment building</dt></def><uro><ure>cliff dwelling</ure><fl>noun</fl></uro></entry></entry_list>');
    }
});