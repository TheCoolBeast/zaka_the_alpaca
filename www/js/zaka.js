//
//   Authors:
//       Daniel Olea Martín <dom.olea@gmail.com>
//       Bruno Ordozgoiti Rubio <ordozgoitipiedras@gmail.com>
//
//   Copyright (c) 2013 Zaka the Alpaca
//    All Rights Reserved.
//
//   BANANAPPS CONFIDENTIAL
//   __________________ 
//   NOTICE:  All information contained herein is, and remains
//   the property of Bananapps and its suppliers,  if any.  The 
//   intellectual and technical concepts contained  herein are 
//   proprietary to Bananapps and its suppliers and may be 
//   covered by U.S. and Foreign Patents, patents in process, 
//   and are protected by trade secret or copyright law.
//   Dissemination of this information or reproduction of this 
//   material is strictly forbidden unless prior written 
//   permission is obtained from Bananapps.


(function(zaka, $, undefined) {
    
    /** Minimum number of words needed to play word parade */
    zaka.MIN_WORDS_WORD_PARADE = 4;
    
    /**
     * Checks whether we are in a phone or a browser.
     * @returns {Boolean} true if we are in a phone, false otherwise.
     */
    zaka.isPhone = function() {
        var isPhone = false;
        if (document.URL.indexOf("http://") === -1
                && document.URL.indexOf("https://") === -1) {
            isPhone = true;
        }
        return isPhone;
    }

    /**
     * Executes the callback once the device is ready (DOM loaded)
     * @param {function} callback
     */
    zaka.onReady = function(callback) {
        $(document).ready(function() {
            // are we running in native app or in browser?
            if (zaka.isPhone()) {
                document.addEventListener("deviceready", callback, false);
            } else {
                callback();
            }
        });
    };

    //TODO: check if its the best way for organizing
    /**
     * Cross-page dictionary (key-value).
     */
    zaka.communication = {
        /**
         * Adds a value by its key.
         * @param {Object} key
         * @param {Object} value
         */
        send: function(key, value) {
            window.localStorage.setItem(key, value);
        },
        /**
         * Gets and removes the value of key.
         * @param {Object} key
         * @returns {Object} value
         */
        receive: function(key) {
            var value = window.localStorage.getItem(key);
            window.localStorage.removeItem(key);
            return value;
        }
    }


}(window.zaka = window.zaka || {}, jQuery));